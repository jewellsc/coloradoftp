# Open source FTP server #

ColoradoFTP is the open source FTP server distributed under the LGPL license. As a Java developer you may shape this server any way you want – replace any parts or add new features easily. The goal of this project was to create an FTP server featuring the possibility to include tasks specific to particular business. Fast and stable FTP server to handle the work you need. Built with the Spring Framework it utilizes all the benefits of transparent application wiring giving you the true freedom to shape the server as you wish. You may extend basic implementations to provide the functionality you require. More than that, there are also plug-ins which you may apply to enhance your FTP server. 

Documentation http://cftp.coldcore.com

License **LGPL**

## master branch ##
* Latest Java version
* New features
* Bigfixes

The master branch contains new features, improvements and bugfixes.

## branch-java5 ##
* Java version 1.5
* No new features
* Bugfixes

This branch was created to support Java5 users with bugfixes and to build Java5 artefacts which you can download from the website. It will unlikely see any new features nor improvements.