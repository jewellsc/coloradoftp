<!--
 Proority:
    ACCESS
      CREATE
        APPEND - files only
          OVERWRITE - files only
            RENAME
              DELETE

  IMPORTANT!
  By default everything is allowed if not stated otherwise.
  To forbid use both "folder-regexp" and "file-regexp" attributes with tag value of 0.
  All regular expressions are tested against lower-cased file/folder names.

  This file was taken from the primary demo FTP server because of the
  advanced configuration of the EMBER user
-->

<!DOCTYPE file-system [
    <!ELEMENT file-system ( users-path, users ) >

    <!ELEMENT users-path ( #PCDATA ) >

    <!ELEMENT users ( user+ ) >
    <!ELEMENT user ( username, home ) >
    <!ATTLIST user default CDATA #IMPLIED >
    <!ELEMENT username ( #PCDATA ) >

    <!ELEMENT home ( properties | virtual-folders )* >
    <!ELEMENT virtual-folders ( folder+ ) >

    <!ELEMENT folder ( name, path, properties* ) >
    <!ELEMENT name ( #PCDATA ) >
    <!ELEMENT path ( #PCDATA ) >

    <!ELEMENT properties ( access | append | create | delete | list | overwrite | rename )* >
    <!ATTLIST properties dir CDATA #IMPLIED >
    <!ATTLIST properties spread CDATA #IMPLIED >

    <!ELEMENT access ( #PCDATA ) >
    <!ATTLIST access file-regexp CDATA #IMPLIED>
    <!ATTLIST access folder-regexp CDATA #IMPLIED >

    <!ELEMENT create ( #PCDATA ) >
    <!ATTLIST create file-regexp CDATA #IMPLIED >
    <!ATTLIST create folder-regexp CDATA #IMPLIED >

    <!ELEMENT append ( #PCDATA ) >
    <!ATTLIST append file-regexp CDATA #REQUIRED >

    <!ELEMENT delete ( #PCDATA ) >
    <!ATTLIST delete file-regexp CDATA #IMPLIED >
    <!ATTLIST delete folder-regexp CDATA #IMPLIED >

    <!ELEMENT list ( #PCDATA ) >
    <!ATTLIST list file-regexp CDATA #IMPLIED >
    <!ATTLIST list folder-regexp CDATA #IMPLIED >

    <!ELEMENT overwrite ( #PCDATA ) >
    <!ATTLIST overwrite file-regexp CDATA #REQUIRED >

    <!ELEMENT rename ( #PCDATA ) >
    <!ATTLIST rename file-regexp CDATA #IMPLIED >
    <!ATTLIST rename folder-regexp CDATA #IMPLIED >


    <!ENTITY fname "[a-zA-Z0-9_\-\. ]+">
    <!ENTITY any ".*">
]>


<file-system>

  <users-path>/ftp/users</users-path>


  <users>

    <!--
    Anonymous user can upload files only to the root folder,
    she cannot create new folders or overwrite files,
    but she can browse sub directories and download anything.
    (no foreigh symbols in path names)
    -->
    <user>
      <username>anonymous</username>

      <home>

        <!-- Do not owerwrite any files in the root folder, do not create folders -->
        <properties>
          <overwrite file-regexp="&any;">0</overwrite>
          <create file-regexp="&fname;">1</create>
          <append file-regexp="&fname;">1</append>
          <create folder-regexp="&any;">0</create>
        </properties>

        <!-- And do not modify content of any sub folder (if any) -->
        <properties spread="1">
          <create folder-regexp="&any;" file-regexp="&any;">0</create>
        </properties>

      </home>
    </user>


    <!-- Ember demos most of the configuration
        (no foreigh symbols in path names) -->
    <user>
      <username>ember</username>

      <home>

        <!-- DIRECTORY: root -->
        <!-- Do not allow anything in the root folder -->
        <properties>
          <create file-regexp="&any;" folder-regexp="&any;">0</create>
        </properties>

        <!-- DIRECTORY: Everything Allowed -->
        <!-- Everything allowed -->
        <properties dir="Everything Allowed" spread="1">
          <create file-regexp="&fname;" folder-regexp="&fname;">1</create>
          <create folder-regexp="&any;" file-regexp="&any;">0</create>
        </properties>

        <!-- DIRECTORY: Infisible Content -->
        <!-- Nothing is visible by everything is allowed, and spread further -->
        <properties dir="Infisible Content" spread="1">
          <create file-regexp="&fname;" folder-regexp="&fname;">1</create>
          <create folder-regexp="&any;" file-regexp="&any;">0</create>
          <list file-regexp="&any;" folder-regexp="&any;">0</list>
        </properties>

        <!-- DIRECTORY: Uploads and Downloads -->
        <!-- Just uploads and downloads, no overwrite -->
        <properties dir="Uploads and Downloads">
          <overwrite file-regexp="&any;">0</overwrite>
          <create file-regexp="&fname;">1</create>
          <append file-regexp="&fname;">1</append>
          <create folder-regexp="&any;">0</create>
        </properties>
        <!-- Only JPG may be uploaded in here -->
        <properties dir="Uploads and Downloads/just JPG uploads">
          <overwrite file-regexp="&any;">0</overwrite>
          <create file-regexp="&fname;.jpg$">1</create>
          <append file-regexp="&fname;.jpg$">1</append>
          <create file-regexp="&fname;">0</create>
          <create folder-regexp="&any;">0</create>
        </properties>
        <!-- Everything but TXT may be uploaded in here -->
        <properties dir="Uploads and Downloads/no TXT uploads">
          <overwrite file-regexp="&any;">0</overwrite>
          <create file-regexp="&fname;.txt$">0</create>
          <create file-regexp="&fname;">1</create>
          <append file-regexp="&fname;">1</append>
          <create folder-regexp="&any;">0</create>
        </properties>

        <!-- Couple of virtual folders outside of the user home -->
        <virtual-folders>

          <folder>
            <name>~Archieve~</name>
            <path>/usr/local/archieve</path>

            <!-- No access to this directory -->
            <properties>
              <access folder-regexp="forbidden">0</access>
            </properties>
            <!-- And allow only downloads from the rest of the directories -->
            <properties spread="1">
              <create file-regexp="&any;" folder-regexp="&any;">0</create>
            </properties>

          </folder>

          <folder>
            <name>~Vault~</name>
            <path>/vault</path>

            <!-- No access to this directory -->
            <properties>
              <access folder-regexp="no access">0</access>
            </properties>
            <properties spread="1">
              <create file-regexp="&any;" folder-regexp="&any;">0</create>
            </properties>
            <!-- And allow only LIST from this directory, no downloads, no uploads -->
            <properties dir="View Only Content" spread="1">
              <access file-regexp="&any;">0</access>
              <create folder-regexp="&any;">0</create>
            </properties>

          </folder>

        </virtual-folders>

      </home>
    </user>

  </users>

</file-system>