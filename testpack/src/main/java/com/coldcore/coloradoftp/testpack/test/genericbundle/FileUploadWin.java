package com.coldcore.coloradoftp.testpack.test.genericbundle;

import org.apache.log4j.Logger;
import com.coldcore.coloradoftp.testpack.test.BaseCommunicatorTest;

/**
 * @see com.coldcore.coloradoftp.testpack.test.genericbundle.FileUpload
 * This shorter test assumes that server saves with \r\n line feeds.
 */
public class FileUploadWin extends BaseCommunicatorTest {

  private static Logger log = Logger.getLogger(FileUploadWin.class);


  protected void setUp() throws Exception {
    beansFilename = "genericbundle-beans-win.xml";
    super.setUp();
  }


  public void testPasvStor() throws Exception {
    anonymousLogin();

    pasv();
    command("stor testfile-1.txt", "150");
    pasvTransfer.writeDataAsString("\n\n\n\nHello there\r\n\r\n");
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-1.txt", "\r\n\r\n\r\n\r\nHello there\r\n\r\n".getBytes());

    command("stor testfile.txt", "425");

    pasv();
    command("stor testfile-1.txt", "150");
    pasvTransfer.writeDataAsString("Hello there,\r\n\n\n\r\nBuy now!\r\n");
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-1.txt", "Hello there,\r\n\r\n\r\n\r\nBuy now!\r\n".getBytes());

    switchToAsciiMode();
    pasv();
    command("stor testfile-2.txt", "150");
    pasvTransfer.writeDataAsString("Hello\nthere,\nBuy now, come again!\n");
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-2.txt", "Hello\r\nthere,\r\nBuy now, come again!\r\n".getBytes());

    switchToBinaryMode();
    pasv();
    command("stor testfile-3.txt", "150");
    pasvTransfer.writeDataAsString("\n\nHello there,\n\nBuy now!\r\n");
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-3.txt", "\n\nHello there,\n\nBuy now!\r\n".getBytes());

    logout();
  }


  public void testPasvStou() throws Exception {
    command("stou", "530");

    anonymousLogin();

    command("stou", "501");

    command("mkd /testdir-A", "257", "250");
    command("cwd /testdir-A", "250");
    command("stou", "425");

    pasv();
    command("stou", "150");
    pasvTransfer.writeDataAsString("\n\n\n\nHello there\r\n\r\n");
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testdir-A/testdir-A", "\r\n\r\n\r\n\r\nHello there\r\n\r\n".getBytes());

    command("stou", "425");

    pasv();
    command("stou", "150");
    pasvTransfer.writeDataAsString("Hello there,\r\n\n\n\r\nBuy now!\r\n");
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testdir-A/testdir-A", "Hello there,\r\n\r\n\r\n\r\nBuy now!\r\n".getBytes());

    switchToAsciiMode();
    pasv();
    command("stou", "150");
    pasvTransfer.writeDataAsString("Hello\nthere,\nBuy now, come again!\n");
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testdir-A/testdir-A", "Hello\r\nthere,\r\nBuy now, come again!\r\n".getBytes());

    switchToBinaryMode();
    pasv();
    command("stou", "150");
    pasvTransfer.writeDataAsString("\n\nHello there,\n\nBuy now!\r\n");
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testdir-A/testdir-A", "\n\nHello there,\n\nBuy now!\r\n".getBytes());

    logout();
  }


  public void testPasvStorUTF8() throws Exception {
    anonymousLogin();

    char[] carr = new char[]{1055,1088,1077,1074,1077,1076,'_',1052,1077,1076,1074,1077,1076};
    String istr = "";
    for (char c : carr)
      istr += c;

    pasv();
    command("stor testfile-1.txt", "150");
    pasvTransfer.writeDataAsString("\n\n\n\n"+istr+"\r\n\r\n");
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-1.txt", ("\r\n\r\n\r\n\r\n"+istr+"\r\n\r\n").getBytes("UTF-8"));

    command("stor testfile.txt", "425");

    pasv();
    command("stor testfile-1.txt", "150");
    pasvTransfer.writeDataAsString(istr+",\n\n\n"+istr+"!\r\n");
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-1.txt", (istr+",\r\n\r\n\r\n"+istr+"!\r\n").getBytes("UTF-8"));

    switchToAsciiMode();
    pasv();
    command("stor testfile-2.txt", "150");
    pasvTransfer.writeDataAsString(istr+"\n"+istr+",\n"+istr+", "+istr+"!\n");
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-2.txt", (istr+"\r\n"+istr+",\r\n"+istr+", "+istr+"!\r\n").getBytes("UTF-8"));

    switchToBinaryMode();
    pasv();
    command("stor testfile-3.txt", "150");
    pasvTransfer.writeDataAsString("\n\n"+istr+",\n\n"+istr+"!\r\n");
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-3.txt", ("\n\n"+istr+",\n\n"+istr+"!\r\n").getBytes("UTF-8"));

    logout();
  }


  public void testPasvStouUTF8() throws Exception {
    anonymousLogin();

    char[] carr = new char[]{1055,1088,1077,1074,1077,1076,'_',1052,1077,1076,1074,1077,1076};
    String istr = "";
    for (char c : carr)
      istr += c;

    command("mkd /testdir-A", "257", "250");
    command("cwd /testdir-A", "250");

    pasv();
    command("stou", "150");
    pasvTransfer.writeDataAsString("\n\n\n\n"+istr+"\r\n\r\n");
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testdir-A/testdir-A", ("\r\n\r\n\r\n\r\n"+istr+"\r\n\r\n").getBytes("UTF-8"));

    command("stor testfile.txt", "425");

    pasv();
    command("stou", "150");
    pasvTransfer.writeDataAsString(istr+",\n\n\n"+istr+"!\r\n");
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testdir-A/testdir-A", (istr+",\r\n\r\n\r\n"+istr+"!\r\n").getBytes("UTF-8"));

    switchToAsciiMode();
    pasv();
    command("stou", "150");
    pasvTransfer.writeDataAsString(istr+"\n"+istr+",\n"+istr+", "+istr+"!\n");
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testdir-A/testdir-A", (istr+"\r\n"+istr+",\r\n"+istr+", "+istr+"!\r\n").getBytes("UTF-8"));

    switchToBinaryMode();
    pasv();
    command("stou", "150");
    pasvTransfer.writeDataAsString("\n\n"+istr+",\n\n"+istr+"!\r\n");
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testdir-A/testdir-A", ("\n\n"+istr+",\n\n"+istr+"!\r\n").getBytes("UTF-8"));

    logout();
  }

}
