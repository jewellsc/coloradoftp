package com.coldcore.coloradoftp.testpack.test.concurrent;

/**
 * Concurrent test with 20 users.
 */
public class ConcurrentAB1 extends ConcurrentAA1 {

    protected void setUp() throws Exception {
        super.setUp("concurrent-beans-prod.xml");
    }

}