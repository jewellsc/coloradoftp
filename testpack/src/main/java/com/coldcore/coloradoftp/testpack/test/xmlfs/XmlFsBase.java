package com.coldcore.coloradoftp.testpack.test.xmlfs;

import com.coldcore.coloradoftp.testpack.test.BaseCommunicatorTest;
import org.apache.log4j.Logger;
import net.sf.cotta.TFileFactory;
import net.sf.cotta.TDirectory;

/**
 * Base class.
 */
abstract public class XmlFsBase extends BaseCommunicatorTest {

  private static Logger log = Logger.getLogger(XmlFsBase.class);


    protected void createRemoteContent() throws Exception {
        super.createRemoteContent();

        TFileFactory fileFactory = getFileFactory();
        TDirectory tdir = fileFactory.dir(remoteUsersPath+"/ember");
        tdir.ensureExists();
    }


    /** Create file system as on the primary FTP demo to demonstrate most
     *  of advanced XML configuration (xmlfs-filesystem-pdemo.xml).
     *  The file and directory names are exactly as on the primary FTP demo.
     */
    protected void createPrimaryDemoRemoteContent() throws Exception {
        TFileFactory fileFactory = getFileFactory();

        fileFactory.dir("/usr/local/archieve").ensureExists();

        fileFactory.dir("/usr/local/archieve/Downloads Only/More Downloads/And Some More").ensureExists();
        createRemoteBinaryFileN(MBx1/100, "/usr/local/archieve/Downloads Only/proboscis_001.psd");
        createRemoteBinaryFileN(MBx1/100, "/usr/local/archieve/Downloads Only/proboscis_002.psd");
        createRemoteBinaryFileN(MBx1/100, "/usr/local/archieve/Downloads Only/proboscis_003.psd");
        createRemoteBinaryFileN(MBx1/100, "/usr/local/archieve/Downloads Only/proboscis_004.psd");
        createRemoteBinaryFileN(MBx1/100, "/usr/local/archieve/Downloads Only/proboscis_005.psd");
        createRemoteBinaryFileN(MBx1/100, "/usr/local/archieve/Downloads Only/More Downloads/double_hearts.jpg");
        createRemoteBinaryFileN(MBx1/100, "/usr/local/archieve/Downloads Only/More Downloads/good_shot.jpg");
        createRemoteBinaryFileN(MBx1/100, "/usr/local/archieve/Downloads Only/More Downloads/good_shot_with_hearts.jpg");
        createRemoteBinaryFileN(MBx1/100, "/usr/local/archieve/Downloads Only/More Downloads/new_born.jpg");
        createRemoteBinaryFileN(MBx1/100, "/usr/local/archieve/Downloads Only/More Downloads/And Some More/good_shot.zip");
        createRemoteBinaryFileN(MBx1/100, "/usr/local/archieve/Downloads Only/More Downloads/And Some More/hello_003_outline.psd");
        createRemoteBinaryFileN(MBx1/100, "/usr/local/archieve/Downloads Only/More Downloads/And Some More/hello_004_outline.psd");
        createRemoteBinaryFileN(MBx1/100, "/usr/local/archieve/Downloads Only/More Downloads/And Some More/smooth_003.jpg");
        createRemoteBinaryFileN(MBx1/100, "/usr/local/archieve/Downloads Only/More Downloads/And Some More/smooth_004.jpg");

        fileFactory.dir("/usr/local/archieve/Forbidden").ensureExists();
        createRemoteBinaryFileN(MBx1/100, "/usr/local/archieve/Forbidden/p3110033.jpg");
        createRemoteBinaryFileN(MBx1/100, "/usr/local/archieve/Forbidden/p3110049.jpg");

        fileFactory.dir("/vault/No Access").ensureExists();
        createRemoteBinaryFileN(MBx1/100, "/vault/No Access/p3110012.jpg");
        createRemoteBinaryFileN(MBx1/100, "/vault/No Access/p3110022.jpg");

        fileFactory.dir("/vault/View Only Content").ensureExists();
        createRemoteBinaryFileN(MBx1/100, "/vault/View Only Content/passwords.dat");
        createRemoteBinaryFileN(MBx1/100, "/vault/View Only Content/secrets.dat");

        fileFactory.dir("/vault/View Only Content/storage 1").ensureExists();
        createRemoteBinaryFileN(MBx1/100, "/vault/View Only Content/storage 1/p3110061.jpg");
        createRemoteBinaryFileN(MBx1/100, "/vault/View Only Content/storage 1/p8190027.jpg");
        createRemoteBinaryFileN(MBx1/100, "/vault/View Only Content/storage 1/p8190034.jpg");

        fileFactory.dir("/vault/View Only Content/storage 2").ensureExists();
        createRemoteBinaryFileN(MBx1/100, "/vault/View Only Content/storage 2/byteofpython_120.txt");
        createRemoteBinaryFileN(MBx1/100, "/vault/View Only Content/storage 2/readme.txt");
        createRemoteBinaryFileN(MBx1/100, "/vault/View Only Content/storage 2/rfc1367.txt");

        fileFactory.dir("/vault/View Only Content/storage 3").ensureExists();
        createRemoteBinaryFileN(MBx1/100, "/vault/View Only Content/storage 3/thumb_048.rar");
        createRemoteBinaryFileN(MBx1/100, "/vault/View Only Content/storage 3/thumb_061.rar");
        createRemoteBinaryFileN(MBx1/100, "/vault/View Only Content/storage 3/thumb_076.rar");
        createRemoteBinaryFileN(MBx1/100, "/vault/View Only Content/storage 3/thumb_082.rar");

        createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/anonymous/COMMANDO.zip");
        createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/anonymous/good_shot_with_hearts.jpg");
        createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/anonymous/hello_003.jpg");

        fileFactory.dir(remoteUsersPath+"/ember/Everything Allowed").ensureExists();
        createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/ember/Everything Allowed/on_the_beach.rar");
        createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/ember/Everything Allowed/spear_warrior_gray.jpg");
        createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/ember/Everything Allowed/spear_warrior_yellow.jpg");

        fileFactory.dir(remoteUsersPath+"/ember/Everything Allowed/My Pictures").ensureExists();
        createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/ember/Everything Allowed/My Pictures/p1210036.jpg");
        createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/ember/Everything Allowed/My Pictures/scarylaser.gif");
        createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/ember/Everything Allowed/My Pictures/UK-Map-l.gif");

        fileFactory.dir(remoteUsersPath+"/ember/Infisible Content").ensureExists();
        createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/ember/Infisible Content/p1060112.jpg");
        createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/ember/Infisible Content/p1210019.jpg");

        fileFactory.dir(remoteUsersPath+"/ember/Infisible Content/Directory A").ensureExists();
        createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/ember/Infisible Content/Directory A/30-09-06_2220.jpg");

        fileFactory.dir(remoteUsersPath+"/ember/Infisible Content/Directory A/My Directory").ensureExists();

        fileFactory.dir(remoteUsersPath+"/ember/Infisible Content/Directory B").ensureExists();

        fileFactory.dir(remoteUsersPath+"/ember/Uploads and Downloads").ensureExists();
        createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/ember/Uploads and Downloads/COMMANDO.rar");
        createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/ember/Uploads and Downloads/new_born_close.jpg");
        createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/ember/Uploads and Downloads/on_the_beach.zip");

        fileFactory.dir(remoteUsersPath+"/ember/Uploads and Downloads/just JPG uploads").ensureExists();
        createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/ember/Uploads and Downloads/just JPG uploads/good_shot_close.jpg");
        createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/ember/Uploads and Downloads/just JPG uploads/hello_003.jpg");
        createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/ember/Uploads and Downloads/just JPG uploads/hello_004.jpg");
        createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/ember/Uploads and Downloads/just JPG uploads/saji.jpg");

        fileFactory.dir(remoteUsersPath+"/ember/Uploads and Downloads/no TXT uploads").ensureExists();
        createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/ember/Uploads and Downloads/no TXT uploads/new_born.jpg");
        createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/ember/Uploads and Downloads/no TXT uploads/new_born_outline.zip");
        createRemoteBinaryFileN(MBx1/100, remoteUsersPath+"/ember/Uploads and Downloads/no TXT uploads/Oldoinyo.gif");
    }


    protected void ensureCannotRename(String path) throws Exception {
        command("rnfr "+path, "350");
        command("rnto "+"abc123-"+path, "550");
    }


    protected void ensureCanRename(String path) throws Exception {
        command("rnfr "+path, "350");
        command("rnto "+"abc123-"+path, "250");
        command("rnfr "+"abc123-"+path, "350");
        command("rnto "+path, "250");
    }


    protected void ensureCanDelete(String path) throws Exception {
        command("dele "+path, "250");
    }


    protected void ensureCannotDelete(String path) throws Exception {
        command("dele "+path, "550");
    }


    protected void ensureCanDownload(String path) throws Exception {
        switchToBinaryMode();
        pasv();
        command("retr "+path, "150");
        pasvTransfer.readData();
        dataTransferComplete();
    }


    protected void ensureCannotDownload(String path) throws Exception {
        switchToBinaryMode();
        String reply = command("retr "+path, "550");
        assertTrue("Allowed: "+reply, reply.contains("Access denied")); //550 if allowed but no PASV sent
    }


    protected void ensureCanUpload(String path) throws Exception {
        switchToBinaryMode();
        pasv();
        command("stor "+path, "150");
        pasvTransfer.writeDataAsString("Hello there");
        dataTransferComplete();
    }


    protected void ensureCannotUpload(String path) throws Exception {
        switchToBinaryMode();
        String reply = command("stor "+path, "550");
        assertTrue("Allowed: "+reply, reply.contains("Access denied")); //550 if allowed but no PASV sent
    }


    protected void ensureCanAppend(String path) throws Exception {
        switchToBinaryMode(); 
        pasv();
        command("appe "+path, "150");
        pasvTransfer.writeDataAsString("Hello there");
        dataTransferComplete();
    }


    protected void ensureCannotAppend(String path) throws Exception {
        switchToBinaryMode();
        String reply = command("appe "+path, "550");
        assertTrue("Allowed: "+reply, reply.contains("Access denied")); //550 if allowed but no PASV sent
    }

    
    protected void ensureCanCreateDirectory(String path) throws Exception {
        command("mkd "+path, "257", "250");
    }


    protected void ensureCannotCreateDirectory(String path) throws Exception {
        command("mkd "+path, "550");
    }


    protected void ensureCanEnterDirectory(String path) throws Exception {
        String pwd = printWorkingDirectory();
        command("cwd "+path, "250");
        command("cwd "+pwd, "250");
    }


    protected void ensureCannotEnterDirectory(String path) throws Exception {
        command("cwd "+path, "550");
    }


    protected void enterDirectory(String path) throws Exception {
        command("cwd "+path, "250");
    }
}