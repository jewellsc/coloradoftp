package com.coldcore.coloradoftp.testpack.test.genericbundle;

import com.coldcore.coloradoftp.testpack.test.BaseCommunicatorTest;
import org.apache.log4j.Logger;

/**
 * Test behaviour of RETR command.
 */
public class FileDownload extends BaseCommunicatorTest {

  private static Logger log = Logger.getLogger(FileDownload.class);


  protected void setUp() throws Exception {
    this.setUp("genericbundle-beans.xml");
  }


  protected void setUp(String beansFilename) throws Exception {
    this.beansFilename = beansFilename;
    super.setUp();
  }


  public void testPasvRetr() throws Exception {
    command("retr", "530");

    anonymousLogin();

    createRemoteBinaryFile(55286, remoteUsersPath+"/anonymous/testfile-1.txt");

    command("retr", "501");
    command("retr testfile.txt", "450");
    command("retr testfile-1.txt", "425");

    switchToBinaryMode();

    pasv();
    command("retr testfile-1.txt", "150");
    byte[] data = pasvTransfer.readData();
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-1.txt", data);

    command("retr testfile-1.txt", "425");

    pasv();
    command("retr testfile-1.txt", "150");
    data = pasvTransfer.readData();
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-1.txt", data);

    logout();
  }


  public void testAsciiBinaryPasvRetr() throws Exception {
    anonymousLogin();

    createRemoteTextFile(223, remoteUsersPath+"/anonymous/testfile-1.txt", false);
    createRemoteTextFile(55286, remoteUsersPath+"/anonymous/testfile-2.txt", false);
    createRemoteTextFile(62435, remoteUsersPath+"/anonymous/testfile-3.txt", true);

    pasv();
    command("retr testfile-1.txt", "150");
    byte[] data = convertToUnixLineFeeds(pasvTransfer.readDataAsString()).getBytes();
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-1.txt", data);

    switchToBinaryMode();
    pasv();
    command("retr testfile-2.txt", "150");
    data = pasvTransfer.readData();
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-2.txt", data);

    switchToAsciiMode();
    pasv();
    command("retr testfile-2.txt", "150");
    data = convertToUnixLineFeeds(pasvTransfer.readDataAsString()).getBytes();
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-2.txt", data);

    pasv();
    command("retr testfile-3.txt", "150");
    data = pasvTransfer.readData();
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-3.txt", data);

    logout();
  }


  public void testPasvRetrSubdir() throws Exception {
    anonymousLogin();

    command("mkd testdir-1", "257", "250");
    createRemoteTextFile(55286, remoteUsersPath+"/anonymous/testdir-1/testfile-1.txt", false);
    createRemoteTextFile(8756, remoteUsersPath+"/anonymous/testdir-1/testfile-2 [bolt].txt", false);

    switchToBinaryMode();

    pasv();
    command("retr /testdir-1/testfile-1.txt", "150");
    byte[] data = pasvTransfer.readData();
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testdir-1/testfile-1.txt", data);

    pasv();
    command("retr testdir-1/testfile-2 [bolt].txt", "150");
    data = pasvTransfer.readData();
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testdir-1/testfile-2 [bolt].txt", data);

    logout();
  }

}
