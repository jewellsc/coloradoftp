package com.coldcore.coloradoftp.testpack.test.concurrent;

import com.coldcore.coloradoftp.testpack.test.BaseNoClientTest;
import com.coldcore.coloradoftp.testpack.core.RandomClient;
import com.coldcore.coloradoftp.testpack.core.RandomCustomClient;

import java.util.Set;
import java.util.LinkedHashSet;

import org.apache.log4j.Logger;

/**
 * Bease test class.
 * All clients connect at the same time and stay online all the time.
 */
abstract public class ConcurrentBase extends BaseNoClientTest {

    private static Logger log = Logger.getLogger(ConcurrentBase.class);
    protected Set<RandomCustomClient> clients;
    protected int maxClients = 20;


    protected void setUp() throws Exception {
        super.setUp();

        clients = new LinkedHashSet<RandomCustomClient>();
        for (int z = 1; z <= maxClients; z++) {
            String username = "ftpuser" + (z < 10 ? "0"+z : ""+z);
            clients.add(new RandomCustomClient(remoteHost, remotePort, username, "ftpuser123"));
        }
    }


    protected void createRemoteContent() throws Exception {
        super.createRemoteContent();

        for (int z = 1; z <= maxClients; z++) {
            String username = "ftpuser" + (z < 10 ? "0" + z : "" + z);
            createUserRemoteContent(username);
        }
    }



    /*** Methods to use in tests ***/


    /** Start all clients */
    protected void startClients() throws Exception {
        for (RandomCustomClient c : clients)
            c.start();
        log.info(maxClients+" clients started");
    }


    /** Stop all clients */
    protected void stopClients() {
        for (RandomCustomClient c : clients)
            c.stop();
        long joinTimeout = System.currentTimeMillis()+10000L;
        for (RandomCustomClient c : clients) {
            c.join();
            if (System.currentTimeMillis() > joinTimeout) {
                log.warn("Some clients faled to stop properly, breaking...");
                break;
            }
        }
        log.info(maxClients+" clients stopped");
    }


    /** Test if any of the clients encountered errors and if so then fail */
    protected void checkErrors() {
        boolean fail = false;
        for (RandomCustomClient c : clients) {
            Set<Throwable> errors = c.getErrors();
            for (Throwable e : errors) {
                log.error("Client '"+c.getUsername()+"' ERROR: "+e.getMessage());
                //log.error(e);  //Print stack trace
                fail = true;
            }
        }
        if (fail) fail("Client errors!");
    }


    /**
     * Count currently running clients
     * @return Amount of running clients
     */
    protected int countAliveClients() {
        int alive = 0;
        for (RandomCustomClient c : clients)
            if (c.isRunning()) alive++;
        return alive;
    }


    /**
     * Wait (and allow clilents to do what they please)
     * @param seconds How many seconds to wait
     */
    protected void waitFor(int seconds) throws Exception {
        long end = System.currentTimeMillis()+(long)seconds*1000L;
        for (int z = 0; z < seconds; z++) {
            long cur = System.currentTimeMillis();
            int alive = countAliveClients();
            if (z%10 == 0) log.info((end-cur)/1000L+" seconds remaining ("+alive+" alive clients)");
            if (alive == 0 || cur >= end) break;
            Thread.sleep(1000);
        }
    }

}