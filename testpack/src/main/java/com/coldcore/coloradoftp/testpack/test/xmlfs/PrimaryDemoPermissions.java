package com.coldcore.coloradoftp.testpack.test.xmlfs;

import org.apache.log4j.Logger;

/**
 * Permissions test (as on the primary demo FTP server)
 */
public class PrimaryDemoPermissions extends XmlFsBase {

    private static Logger log = Logger.getLogger(PrimaryDemoPermissions.class);


    protected void setUp() throws Exception {
        beansFilename = "xmlfs-beans-pdemo.xml";
        super.setUp();
    }


    protected void createRemoteContent() throws Exception {
        super.createRemoteContent();
        createPrimaryDemoRemoteContent();
    }


    public void testAnonymous() throws Exception {
        anonymousLogin();

        ensureWorkingDirectoryMatches("/");
        ensureListedInWorkingDirectory("COMMANDO.zip");
        ensureListedInWorkingDirectory("good_shot_with_hearts.jpg");
        ensureListedInWorkingDirectory("hello_003.jpg");

        String item = "COMMANDO.zip";
        ensureCannotRename(item);
        ensureCannotDelete(item);
        ensureCanDownload(item);
        ensureCannotUpload(item);
        ensureCanAppend(item);

        ensureCanUpload("testfile.txt");
        ensureCannotUpload("testfile"+(char)1087+(char)1088+".txt");

        ensureCannotCreateDirectory("testdir");

        logout();
    }

    /** Test the root folder */
    public void testEmber01() throws Exception {
        login("ember", "ember");

        ensureWorkingDirectoryMatches("/");
        ensureListedInWorkingDirectory("Everything Allowed");
        ensureListedInWorkingDirectory("Infisible Content");
        ensureListedInWorkingDirectory("Uploads and Downloads");
        ensureListedInWorkingDirectory("~Archieve~");
        ensureListedInWorkingDirectory("~Vault~");

        ensureCannotUpload("testfile.txt");
        ensureCannotCreateDirectory("testdir");

        String item = "Everything Allowed";
        ensureCanEnterDirectory(item);
        ensureCannotRename(item);
        ensureCannotDelete(item);

        item = "~Archieve~";
        ensureCanEnterDirectory(item);
        ensureCannotRename(item);
        ensureCannotDelete(item);

        logout();
    }


    /** Test the "Everything Allowed" folder and beyond */
    public void testEmber02() throws Exception {
        login("ember", "ember");

        enterDirectory("Everything Allowed");
        ensureWorkingDirectoryMatches("/Everything Allowed");

        ensureListedInWorkingDirectory("My Pictures");
        ensureListedInWorkingDirectory("on_the_beach.rar");
        ensureListedInWorkingDirectory("spear_warrior_gray.jpg");
        ensureListedInWorkingDirectory("spear_warrior_yellow.jpg");

        String item = "testfile.txt";
        ensureCanUpload(item);
        ensureCanAppend(item);
        ensureCanDownload(item);
        ensureCanRename(item);
        ensureCanDelete(item);

        item = "testdir";
        ensureCanCreateDirectory(item);
        ensureCanEnterDirectory(item);
        ensureCanRename(item);
        ensureCanDelete(item);

        item = "My Pictures";
        ensureCanEnterDirectory(item);
        ensureCanRename(item);
        ensureCanDelete(item);

        item = "on_the_beach.rar";
        ensureCanUpload(item);
        ensureCanAppend(item);
        ensureCanDownload(item);
        ensureCanRename(item);
        ensureCanDelete(item);

        logout();
    }


    /** Test the "Infisible Content" folder and beyond */
    public void testEmber03() throws Exception {
        login("ember", "ember");

        enterDirectory("Infisible Content");
        ensureWorkingDirectoryMatches("/Infisible Content");

        ensureNothingListedInWorkingDirectory();

        ensureCanEnterDirectory("Directory A");
        ensureCanDownload("p1060112.jpg");

        enterDirectory("Directory A");
        ensureWorkingDirectoryMatches("/Infisible Content/Directory A");

        ensureNothingListedInWorkingDirectory();

        ensureCanEnterDirectory("My Directory");
        ensureCanDownload("30-09-06_2220.jpg");

        logout();
    }


    /** Test the "Uploads and Downloads" folder and beyond */
    public void testEmber04() throws Exception {
        login("ember", "ember");

        enterDirectory("Uploads and Downloads");
        ensureWorkingDirectoryMatches("/Uploads and Downloads");

        ensureListedInWorkingDirectory("just JPG uploads");
        ensureListedInWorkingDirectory("no TXT uploads");
        ensureListedInWorkingDirectory("new_born_close.jpg");

        String item = "testfile.txt";
        ensureCanUpload(item);
        ensureCanAppend(item);
        ensureCanDownload(item);
        ensureCannotRename(item);
        ensureCannotDelete(item);
        ensureCannotUpload(item); //2nd upload is an Overwrite of the 1st upload

        ensureCannotCreateDirectory("testdir");
        ensureCannotRename("just JPG uploads");
        ensureCannotDelete("just JPG uploads");

        enterDirectory("just JPG uploads");

        ensureListedInWorkingDirectory("hello_004.jpg");

        item = "hello_004.jpg";
        ensureCannotRename(item);
        ensureCannotDelete(item);
        ensureCannotUpload(item);

        ensureCanUpload("testfile1.jpg");
        ensureCanUpload("testfile2.JPG");
        ensureCannotUpload("testfile3.txt");
        ensureCannotUpload("testfile4.MPG");

        enterDirectory("../no TXT uploads");

        ensureListedInWorkingDirectory("new_born.jpg");

        item = "new_born.jpg";
        ensureCannotRename(item);
        ensureCannotDelete(item);
        ensureCannotUpload(item);
        
        ensureCanUpload("testfile1.jpg");
        ensureCanUpload("testfile2.JPG");
        ensureCannotUpload("testfile3.txt");
        ensureCanUpload("testfile4.MPG");

        logout();
    }


    /** Test the root of the "~Archieve~" folder */
    public void testEmber05() throws Exception {
        login("ember", "ember");

        enterDirectory("~Archieve~");
        ensureWorkingDirectoryMatches("/~Archieve~");

        ensureListedInWorkingDirectory("Downloads Only");
        ensureListedInWorkingDirectory("Forbidden");

        ensureCannotUpload("testfile.txt");
        ensureCannotCreateDirectory("testdir");

        String item = "Downloads Only";
        ensureCannotRename(item);
        ensureCannotDelete(item);
        ensureCanEnterDirectory(item);

        item = "Forbidden";
        ensureCannotEnterDirectory(item);
        ensureCannotRename(item);
        ensureCannotDelete(item);

        logout();
    }


    /** Test the "~Archieve~/Downloads Only" folder and beyond */
    public void testEmber06() throws Exception {
        login("ember", "ember");

        enterDirectory("~Archieve~/Downloads Only");
        ensureWorkingDirectoryMatches("/~Archieve~/Downloads Only");

        ensureListedInWorkingDirectory("More Downloads");
        ensureListedInWorkingDirectory("proboscis_001.psd");

        ensureCannotUpload("testfile.txt");
        ensureCannotCreateDirectory("testdir");

        String item = "More Downloads";
        ensureCannotRename(item);
        ensureCannotDelete(item);
        ensureCanEnterDirectory(item);

        item = "proboscis_001.psd";
        ensureCannotUpload(item);
        ensureCannotAppend(item);
        ensureCannotDelete(item);
        ensureCannotRename(item);
        ensureCanDownload(item);

        enterDirectory("More Downloads");

        ensureListedInWorkingDirectory("And Some More");
        ensureListedInWorkingDirectory("double_hearts.jpg");

        ensureCannotUpload("testfile.txt");
        ensureCannotCreateDirectory("testdir");

        ensureCannotRename("And Some More");

        item = "double_hearts.jpg";
        ensureCannotUpload(item);
        ensureCannotAppend(item);
        ensureCannotDelete(item);
        ensureCannotRename(item);
        ensureCanDownload(item);

        logout();
    }


    /** Test the root of the "~Vault~" folder */
    public void testEmber07() throws Exception {
        login("ember", "ember");

        enterDirectory("~Vault~");
        ensureWorkingDirectoryMatches("/~Vault~");

        ensureListedInWorkingDirectory("No Access");
        ensureListedInWorkingDirectory("View Only Content");

        ensureCannotUpload("testfile.txt");
        ensureCannotCreateDirectory("testdir");

        String item = "View Only Content";
        ensureCannotRename(item);
        ensureCannotDelete(item);
        ensureCanEnterDirectory(item);

        item = "No Access";
        ensureCannotEnterDirectory(item);
        ensureCannotRename(item);
        ensureCannotDelete(item);

        logout();
    }


    /** Test the "~Vault~/View Only Content" folder and beyond */
    public void testEmber08() throws Exception {
        login("ember", "ember");

        enterDirectory("~Vault~/View Only Content");
        ensureWorkingDirectoryMatches("/~Vault~/View Only Content");

        ensureListedInWorkingDirectory("storage 1");
        ensureListedInWorkingDirectory("storage 2");
        ensureListedInWorkingDirectory("storage 3");
        ensureListedInWorkingDirectory("passwords.dat");
        ensureListedInWorkingDirectory("secrets.dat");

        ensureCannotUpload("testfile.txt");
        ensureCannotCreateDirectory("testdir");

        String item = "storage 1";
        ensureCannotRename(item);
        ensureCannotDelete(item);
        ensureCanEnterDirectory(item);

        item = "passwords.dat";
        ensureCannotUpload(item);
        ensureCannotAppend(item);
        ensureCannotDelete(item);
        ensureCannotRename(item);
        ensureCannotDownload(item);

        enterDirectory("storage 1");

        ensureListedInWorkingDirectory("p3110061.jpg");
        ensureListedInWorkingDirectory("p8190027.jpg");
        ensureListedInWorkingDirectory("p8190034.jpg");
        
        ensureCannotUpload("testfile.txt");
        ensureCannotCreateDirectory("testdir");

        item = "p3110061.jpg";
        ensureCannotUpload(item);
        ensureCannotAppend(item);
        ensureCannotDelete(item);
        ensureCannotRename(item);
        ensureCannotDownload(item);

        logout();
    }

}