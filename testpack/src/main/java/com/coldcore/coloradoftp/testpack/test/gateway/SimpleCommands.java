package com.coldcore.coloradoftp.testpack.test.gateway;

import com.coldcore.coloradoftp.testpack.test.BaseCommunicatorTest;

/**
 * Test replies of the most simple commands.
 */
public class SimpleCommands extends BaseCommunicatorTest {

  protected void setUp() throws Exception {
    beansFilename = "gateway-beans.xml";
    super.setUp();
  }


  public void testAnonymousLogin() throws Exception {
    anonymousLogin();
    logout();
  }


  public void testConfiguredLogin() throws Exception {
    login("ftpuser", "ftpuser123");
    logout();
  }


  public void testUserPass() throws Exception {
    command("quit", "530");

    command("pass crazy", "503");
    command("user", "501");

    command("user crazy", "331");
    command("pass", "501");

    command("user crazy", "331");
    command("pass crazy", "530");

    command("user anonymous", "331");
    command("pass ano@nym.ous", "230");

    command("user crazy", "503");
    command("pass crazy", "503");
  }

}