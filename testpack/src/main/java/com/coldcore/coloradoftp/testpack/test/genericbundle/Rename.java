package com.coldcore.coloradoftp.testpack.test.genericbundle;

import com.coldcore.coloradoftp.testpack.test.BaseCommunicatorTest;
import org.apache.log4j.Logger;

/**
 * Test if server properly renames items.
 * Testing RNFR, RNTO commands
 */
public class Rename extends BaseCommunicatorTest {

  private static Logger log = Logger.getLogger(Rename.class);


  protected void setUp() throws Exception {
    beansFilename = "genericbundle-beans.xml";
    super.setUp();
  }


  public void testRename() throws Exception {
    command("rnfr", "530");
    command("rnto", "530");

    anonymousLogin();

    command("mkd testdir-1", "257", "250");
    command("mkd testdir-1/testdir-2", "257", "250");
    command("mkd testdir-1/testdir-2/testdir-3", "257", "250");

    createRemoteTextFile(112, remoteUsersPath+"/anonymous/testfile.txt", false);
    createRemoteTextFile(1024, remoteUsersPath+"/anonymous/testdir-1/testfile-1.txt", false);
    createRemoteTextFile(256, remoteUsersPath+"/anonymous/testdir-1/testfile-2.txt", false);
    createRemoteTextFile(16, remoteUsersPath+"/anonymous/testdir-1/testfile-3.txt", false);
    createRemoteTextFile(54637, remoteUsersPath+"/anonymous/testdir-1/testdir-2/testfile-4.txt", false);

    ensureListedInWorkingDirectory("testfile.txt", "testdir-1");

    command("rnfr", "501");
    command("rnto", "501");
    command("rnto testfile-N.txt", "503");

    command("rnfr /", "350");
    command("rnto crazy", "553");


    command("rnfr testfile.txt", "350");
    command("rnto testfile-N.txt", "250");
    ensureListedInWorkingDirectory("testfile-N.txt", "testdir-1");
    ensureNotListedInWorkingDirectory("testfile.txt");

    command("rnto testfile-N.txt", "503");
    command("rnfr testfile.txt", "350");
    command("rnto testfile-N.txt", "450");


    command("cwd testdir-1", "250");
    ensureListedInWorkingDirectory("testfile-1.txt", "testfile-2.txt", "testfile-3.txt", "testdir-2");
    command("rnfr testfile-1.txt", "350");
    command("rnto testfile-1-N.txt", "250");
    command("rnfr testfile-2.txt", "350");
    command("rnto testfile-2-N.txt", "250");
    command("rnfr testdir-2", "350");
    command("rnto testdir-2-N", "250");
    ensureListedInWorkingDirectory("testfile-1-N.txt", "testfile-2-N.txt", "testfile-3.txt", "testdir-2-N");
    ensureNotListedInWorkingDirectory("testfile-1.txt", "testfile-2.txt", "testdir-2.txt");

    command("cwd testdir-2-N", "250");
    ensureListedInWorkingDirectory("testfile-4.txt", "testdir-3");
    command("rnfr testfile-4.txt", "350");
    command("rnto testfile-4.txt", "450");
    command("rnfr testdir-3", "350");
    command("rnto testdir-3", "450");
    ensureListedInWorkingDirectory("testfile-4.txt", "testdir-3");
    command("rnfr testfile-4.txt", "350");
    command("rnto testdir-3", "450");
    command("rnfr testdir-3", "350");
    command("rnto testfile-4.txt", "450");
    ensureListedInWorkingDirectory("testfile-4.txt", "testdir-3");

    checkRemoteFileSize(remoteUsersPath+"/anonymous/testdir-1/testfile-2-N.txt", 256);
    checkRemoteFileSize(remoteUsersPath+"/anonymous/testdir-1/testfile-1-N.txt", 1024);
    checkRemoteFileSize(remoteUsersPath+"/anonymous/testdir-1/testdir-2-N/testfile-4.txt", 54637);
    checkRemoteFileSize(remoteUsersPath+"/anonymous/testdir-1/testfile-3.txt", 16);
    checkRemoteFileSize(remoteUsersPath+"/anonymous/testfile-N.txt", 112);

    logout();
  }


  public void testRenameSubDir() throws Exception {
    anonymousLogin();

    command("mkd testdir-1", "257", "250");
    command("mkd testdir-2", "257", "250");
    command("mkd testdir-2/testdir-3", "257", "250");

    createRemoteTextFile(112, remoteUsersPath+"/anonymous/testfile.txt", false);
    createRemoteTextFile(1024, remoteUsersPath+"/anonymous/testdir-1/testfile-1.txt", false);
    createRemoteTextFile(256, remoteUsersPath+"/anonymous/testdir-1/testfile-2.txt", false);
    createRemoteTextFile(16, remoteUsersPath+"/anonymous/testdir-1/testfile-3.txt", false);
    createRemoteTextFile(1024, remoteUsersPath+"/anonymous/testdir-2/testfile-A.txt", false);
    createRemoteTextFile(256, remoteUsersPath+"/anonymous/testdir-2/testdir-3/testfile-B.txt", false);
    createRemoteTextFile(32003, remoteUsersPath+"/anonymous/testdir-2/testdir-3/testfile-C.txt", false);

    command("rnfr /testdir-1/testfile-1.txt", "350");
    command("rnto /testdir-1/testfile-1-N.txt", "250");
    command("rnfr testdir-1/testfile-2.txt", "350");
    command("rnto testfile-2-N.txt", "250");
    ensureListedInWorkingDirectory("testfile.txt", "testfile-2-N.txt", "testdir-1", "testdir-2");
    command("rnfr /testfile.txt", "350");
    command("rnto /testdir-1/testfile.txt", "250");
    ensureNotListedInWorkingDirectory("testfile.txt");
    command("cwd testdir-1", "250");
    ensureListedInWorkingDirectory("testfile.txt", "testfile-1-N.txt", "testfile-3.txt");
    ensureNotListedInWorkingDirectory("testfile-2.txt", "testfile-2-N.txt");

    command("cwd /testdir-2", "250");
    command("rnfr ../testdir-1/testfile.txt", "350");
    command("rnto testfile.txt", "250");
    command("rnfr testfile-A.txt", "350");
    command("rnto testdir-3/../testdir-3/testfile-A.txt", "250");
    command("rnfr testdir-3/testfile-B.txt", "350");
    command("rnto testdir-3/../testfile-B.txt", "250");
    command("rnfr /testdir-2/testdir-3/testfile-C.txt", "350");
    command("rnto testdir-3/../../testdir-2/testfile-C.txt", "250");
    ensureListedInWorkingDirectory("testfile.txt", "testfile-B.txt", "testfile-C.txt", "testdir-3");
    ensureNotListedInWorkingDirectory("testfile-A.txt");

    logout();
  }


  public void testRenameSpace() throws Exception {
    anonymousLogin();

    command("mkd testdir-2 [bolt]", "257", "250");
    command("mkd testdir-2 [bolt]/testdir-3 [bolt]", "257", "250");

    createRemoteTextFile(1024, remoteUsersPath+"/anonymous/testdir-2 [bolt]/testfile-A [bolt].txt", false);

    command("cwd testdir-2 [bolt]", "250");

    ensureListedInWorkingDirectory("testfile-A [bolt].txt", "testdir-3 [bolt]");
    ensureNotListedInWorkingDirectory("testfile-B [bolt].txt", "testdir-4 [bolt]");
    command("rnfr testfile-A [bolt].txt", "350");
    command("rnto testfile-B [bolt].txt", "250");
    command("rnfr testdir-3 [bolt]", "350");
    command("rnto testdir-4 [bolt]", "250");
    ensureListedInWorkingDirectory("testfile-B [bolt].txt", "testdir-4 [bolt]");
    ensureNotListedInWorkingDirectory("testfile-A [bolt].txt", "testdir-3 [bolt]");

    logout();
  }


  public void testListingSubDirSpace() throws Exception {
    anonymousLogin();

    command("mkd testdir-2 [bolt]", "257", "250");
    command("mkd testdir-2 [bolt]/testdir-3 [bolt]", "257", "250");

    createRemoteTextFile(1024, remoteUsersPath+"/anonymous/testdir-2 [bolt]/testfile-A [bolt].txt", false);

    command("rnfr testdir-2 [bolt]/testfile-A [bolt].txt", "350");
    command("rnto testdir-2 [bolt]/testdir-3 [bolt]/testfile-A [bolt].txt", "250");
    command("rnfr testdir-2 [bolt]/testdir-3 [bolt]", "350");
    command("rnto testdir-2 [bolt]/testdir-4 [bolt]", "250");

    command("cwd testdir-2 [bolt]", "250");
    ensureListedInWorkingDirectory("testdir-4 [bolt]");
    ensureNotListedInWorkingDirectory("testfile-A [bolt].txt", "testdir-3 [bolt]");

    command("cwd testdir-4 [bolt]", "250");
    ensureListedInWorkingDirectory("testfile-A [bolt].txt");


    logout();
  }


  public void testRenameCurDir() throws Exception {
    anonymousLogin();

    command("mkd testdir-2", "257", "250");


    command("cwd testdir-2", "250");
    ensureWorkingDirectoryMatches("/testdir-2");

    command("rnfr /testdir-2", "350");
    command("rnto /testdir-3", "250");

    command("cdup", "250");
    command("cwd testdir-2", "450");
    command("cwd testdir-3", "250");
    ensureWorkingDirectoryMatches("/testdir-3");

    logout();
  }
}
