package com.coldcore.coloradoftp.testpack.test.genericbundle.special;

import com.coldcore.coloradoftp.testpack.test.BaseCommunicatorTest;
import org.apache.log4j.Logger;

/**
 * Test if server does not hold PASV data ports. Once connection established a data port
 * must accept a new connection. The same connection must not occupy all data ports by
 * sending PASV continiously.
 */
public class FreePasvDataPorts extends BaseCommunicatorTest {

  private static Logger log = Logger.getLogger(FreePasvDataPorts.class);


  protected void setUp() throws Exception {
    beansFilename = "genericbundle-beans.xml";
    super.setUp();
  }


  public void testPasvSingleConnection() throws Exception {
    anonymousLogin();

    for (int z = 0; z < 15; z++)
      command("pasv", "227");

    communicator.send("quit");
    nextReplyCode("425");
    nextReplyCode("221");
  }

}
