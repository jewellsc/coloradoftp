package com.coldcore.coloradoftp.testpack.core;

import com.coldcore.coloradoftp.core.Core;
import com.coldcore.coloradoftp.factory.ObjectFactory;
import com.coldcore.coloradoftp.factory.ObjectName;
import com.coldcore.coloradoftp.factory.impl.SpringFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

/**
 * A simple class to start and stop an FTP server.
 */
public class Launcher {

  protected BeanFactory beanFactory;
  protected Core core;


  public BeanFactory getBeanFactory() {
    return beanFactory;
  }


  public void setBeanFactory(BeanFactory beanFactory) {
    this.beanFactory = beanFactory;
  }


  /**
   * Initialise
   */
  public void initialize() {
    ObjectFactory.setInternalFactory(new SpringFactory(beanFactory));
    core = (Core) ObjectFactory.getObject(ObjectName.CORE);
  }


  public Core getCore() {
    return core;
  }


  public void startCore() {
    if (core == null) throw new IllegalStateException("Core is not initialised");
    core.start();
  }


  public void stopCore() {
    if (core == null) throw new IllegalStateException("Core is not initialised");
    core.stop();
  }


  public void poisonCore() {
    if (core == null) throw new IllegalStateException("Core is not initialised");
    core.poison();
  }


  public static XmlBeanFactory createClasspathXmlBeanFactory(String filename) {
    Resource resource = new ClassPathResource(filename);
    return new XmlBeanFactory(resource);
  }


  public static ApplicationContext createApplicationContext(String... filenames) {
    return new FileSystemXmlApplicationContext(filenames);
  }
}