package com.coldcore.coloradoftp.testpack.test.genericbundle;

import com.coldcore.coloradoftp.testpack.test.BaseCommunicatorTest;
import org.apache.log4j.Logger;

/**
 * Test ascii/binary file transfer (real files ASCII/UTF-8).
 */
public class TransferTypeAI extends BaseCommunicatorTest {

  private static Logger log = Logger.getLogger(TransferTypeAI.class);


  protected void setUp() throws Exception {
    beansFilename = "genericbundle-beans.xml";
    super.setUp();

    copyClasspathFileToRemoteFileSystem("testfile-rfc959-ascii-unix.txt", remoteUsersPath+"/anonymous");
    copyClasspathFileToRemoteFileSystem("testfile-rfc959-ascii-win.txt", remoteUsersPath+"/anonymous");
    copyClasspathFileToRemoteFileSystem("testfile-pooh-utf8-unix.txt", remoteUsersPath+"/anonymous");
    copyClasspathFileToRemoteFileSystem("testfile-pooh-utf8-win.txt", remoteUsersPath+"/anonymous");
  }


  public void testBinaryRetr() throws Exception {
    anonymousLogin();

    byte[] data;
    String filename;

    switchToBinaryMode();

    pasv();
    filename = "testfile-rfc959-ascii-unix.txt";
    command("retr "+filename, "150");
    data = pasvTransfer.readData();
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/"+filename, data);

    pasv();
    filename = "testfile-rfc959-ascii-win.txt";
    command("retr "+filename, "150");
    data = pasvTransfer.readData();
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/"+filename, data);

    pasv();
    filename = "testfile-pooh-utf8-unix.txt";
    command("retr "+filename, "150");
    data = pasvTransfer.readData();
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/"+filename, data);

    pasv();
    filename = "testfile-pooh-utf8-win.txt";
    command("retr "+filename, "150");
    data = pasvTransfer.readData();
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/"+filename, data);

    logout();
  }


  public void testAsciiRetr() throws Exception {
    anonymousLogin();

    byte[] data;

    switchToAsciiMode();

    pasv();
    command("retr testfile-rfc959-ascii-unix.txt", "150");
    data = pasvTransfer.readData();
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-rfc959-ascii-win.txt", data);

    pasv();
    command("retr testfile-rfc959-ascii-win.txt", "150");
    data = pasvTransfer.readData();
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-rfc959-ascii-win.txt", data);

    pasv();
    command("retr testfile-pooh-utf8-unix.txt", "150");
    data = pasvTransfer.readData();
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-pooh-utf8-win.txt", data);

    pasv();
    command("retr testfile-pooh-utf8-win.txt", "150");
    data = pasvTransfer.readData();
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-pooh-utf8-win.txt", data);

    logout();
  }



  public void testBinaryStor() throws Exception {
    anonymousLogin();

    byte[] data;

    switchToBinaryMode();

    data = readRemoteFile(remoteUsersPath+"/anonymous/testfile-rfc959-ascii-unix.txt");
    pasv();
    command("stor testfile-rfc959-ascii-unix_2.txt", "150");
    pasvTransfer.writeData(data);
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-rfc959-ascii-unix_2.txt", data);

    data = readRemoteFile(remoteUsersPath+"/anonymous/testfile-rfc959-ascii-win.txt");
    pasv();
    command("stor testfile-rfc959-ascii-win_2.txt", "150");
    pasvTransfer.writeData(data);
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-rfc959-ascii-win_2.txt", data);

    data = readRemoteFile(remoteUsersPath+"/anonymous/testfile-pooh-utf8-unix.txt");
    pasv();
    command("stor testfile-pooh-utf8-unix_2.txt", "150");
    pasvTransfer.writeData(data);
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-pooh-utf8-unix_2.txt", data);

    data = readRemoteFile(remoteUsersPath+"/anonymous/testfile-pooh-utf8-win.txt");
    pasv();
    command("stor testfile-pooh-utf8-win_2.txt", "150");
    pasvTransfer.writeData(data);
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-pooh-utf8-win_2.txt", data);

    logout();
  }


  public void testAsciiStor() throws Exception {
    anonymousLogin();

    byte[] data;
    byte[] data2;

    switchToAsciiMode();

    data = readRemoteFile(remoteUsersPath+"/anonymous/testfile-rfc959-ascii-unix.txt");
    pasv();
    command("stor testfile-rfc959-ascii-unix_2.txt", "150");
    pasvTransfer.writeData(data);
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-rfc959-ascii-unix_2.txt", data);

    data = readRemoteFile(remoteUsersPath+"/anonymous/testfile-rfc959-ascii-win.txt");
    data2 = readRemoteFile(remoteUsersPath+"/anonymous/testfile-rfc959-ascii-unix.txt");
    pasv();
    command("stor testfile-rfc959-ascii-win_2.txt", "150");
    pasvTransfer.writeData(data);
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-rfc959-ascii-win_2.txt", data2);

    data = readRemoteFile(remoteUsersPath+"/anonymous/testfile-pooh-utf8-unix.txt");
    pasv();
    command("stor testfile-pooh-utf8-unix_2.txt", "150");
    pasvTransfer.writeData(data);
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-pooh-utf8-unix_2.txt", data);

    data = readRemoteFile(remoteUsersPath+"/anonymous/testfile-pooh-utf8-win.txt");
    data2 = readRemoteFile(remoteUsersPath+"/anonymous/testfile-pooh-utf8-unix.txt");
    pasv();
    command("stor testfile-pooh-utf8-win_2.txt", "150");
    pasvTransfer.writeData(data);
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-pooh-utf8-win_2.txt", data2);

    logout();
  }


  public void testBinaryAppe() throws Exception {
    anonymousLogin();

    byte[] data;

    switchToBinaryMode();

    data = readRemoteFile(remoteUsersPath+"/anonymous/testfile-rfc959-ascii-unix.txt");
    pasv();
    command("appe testfile-rfc959-ascii-unix_2.txt", "150");
    pasvTransfer.writeData(data);
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-rfc959-ascii-unix_2.txt", data);

    data = readRemoteFile(remoteUsersPath+"/anonymous/testfile-rfc959-ascii-win.txt");
    pasv();
    command("appe testfile-rfc959-ascii-win_2.txt", "150");
    pasvTransfer.writeData(data);
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-rfc959-ascii-win_2.txt", data);

    data = readRemoteFile(remoteUsersPath+"/anonymous/testfile-pooh-utf8-unix.txt");
    pasv();
    command("appe testfile-pooh-utf8-unix_2.txt", "150");
    pasvTransfer.writeData(data);
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-pooh-utf8-unix_2.txt", data);

    data = readRemoteFile(remoteUsersPath+"/anonymous/testfile-pooh-utf8-win.txt");
    pasv();
    command("appe testfile-pooh-utf8-win_2.txt", "150");
    pasvTransfer.writeData(data);
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testfile-pooh-utf8-win_2.txt", data);

    logout();
  }


  public void testAsciiAppe() throws Exception {
    anonymousLogin();

    switchToAsciiMode();

    command("appe testfile-rfc959-ascii-unix_2.txt", "550");

    logout();
  }


  public void testBinaryStou() throws Exception {
    anonymousLogin();

    byte[] data;

    switchToBinaryMode();

    command("mkd testdir", "257", "250");
    command("cwd testdir", "250");

    data = readRemoteFile(remoteUsersPath+"/anonymous/testfile-rfc959-ascii-unix.txt");
    pasv();
    command("stou", "150");
    pasvTransfer.writeData(data);
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testdir/testdir", data);

    data = readRemoteFile(remoteUsersPath+"/anonymous/testfile-rfc959-ascii-win.txt");
    pasv();
    command("stou", "150");
    pasvTransfer.writeData(data);
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testdir/testdir", data);

    data = readRemoteFile(remoteUsersPath+"/anonymous/testfile-pooh-utf8-unix.txt");
    pasv();
    command("stou", "150");
    pasvTransfer.writeData(data);
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testdir/testdir", data);

    data = readRemoteFile(remoteUsersPath+"/anonymous/testfile-pooh-utf8-win.txt");
    pasv();
    command("stou", "150");
    pasvTransfer.writeData(data);
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testdir/testdir", data);

    logout();
  }


  public void testAsciiStou() throws Exception {
    anonymousLogin();

    byte[] data;
    byte[] data2;

    switchToAsciiMode();

    command("mkd testdir", "257", "250");
    command("cwd testdir", "250");

    data = readRemoteFile(remoteUsersPath+"/anonymous/testfile-rfc959-ascii-unix.txt");
    pasv();
    command("stou", "150");
    pasvTransfer.writeData(data);
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testdir/testdir", data);

    data = readRemoteFile(remoteUsersPath+"/anonymous/testfile-rfc959-ascii-win.txt");
    data2 = readRemoteFile(remoteUsersPath+"/anonymous/testfile-rfc959-ascii-unix.txt");
    pasv();
    command("stou", "150");
    pasvTransfer.writeData(data);
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testdir/testdir", data2);

    data = readRemoteFile(remoteUsersPath+"/anonymous/testfile-pooh-utf8-unix.txt");
    pasv();
    command("stou", "150");
    pasvTransfer.writeData(data);
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testdir/testdir", data);

    data = readRemoteFile(remoteUsersPath+"/anonymous/testfile-pooh-utf8-win.txt");
    data2 = readRemoteFile(remoteUsersPath+"/anonymous/testfile-pooh-utf8-unix.txt");
    pasv();
    command("stou", "150");
    pasvTransfer.writeData(data);
    dataTransferComplete();
    checkRemoteBinaryFileIntegrity(remoteUsersPath+"/anonymous/testdir/testdir", data2);

    logout();
  }
}
