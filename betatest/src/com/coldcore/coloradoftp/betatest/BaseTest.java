package com.coldcore.coloradoftp.betatest;

import com.coldcore.coloradoftp.factory.ObjectFactory;
import junit.framework.TestCase;
import net.sf.cotta.TDirectory;
import net.sf.cotta.TFileFactory;
import org.apache.commons.net.ftp.FTPClient;
import org.springframework.beans.factory.BeanFactory;

/**
 * This is an example base for test files.
 */
abstract public class BaseTest extends TestCase {

  protected String username = "anonymous";
  protected String password = "ano@nym.ous";
  protected String remoteHost = "127.0.0.1";
  protected int remotePort = 21;
  protected String remoteUsersPath = "/ftp/users";
  protected String fileFactoryName = "cottaFileFactory";
  protected String beansFilename = "beans.xml";
  protected Launcher launcher;
  protected FTPClient ftpClient;


  protected void setUp() throws Exception {
    initializeFtpServer();
    startFtpServer();
    Thread.sleep(100);
    createRemoteContent();
    initializeFtpClient();
    connectFtpClient();
    Thread.sleep(100);
  }


  protected void tearDown() throws Exception {
    disconnectFtpClient();
    stopFtpServer();
    Thread.sleep(100);
  }


  protected void initializeFtpServer() {
    if (launcher == null) {
      launcher = new Launcher();
      BeanFactory bf = Launcher.createClasspathXmlBeanFactory(beansFilename);
      launcher.setBeanFactory(bf);
      launcher.initialize();
    }
  }


  protected void initializeFtpClient() {
    if (ftpClient == null) {
      ftpClient = new FTPClient();
    }
  }


  protected void startFtpServer() throws Exception {
    if (launcher != null) launcher.startCore();
    else throw new Exception("No launcher");
  }


  protected void stopFtpServer() throws Exception {
    if (launcher != null) launcher.stopCore();
    else throw new Exception("No launcher");
  }


  protected void connectFtpClient() throws Exception {
    if (ftpClient == null) throw new Exception("No client");
    ftpClient.connect(remoteHost, remotePort);
  }


  protected void disconnectFtpClient() throws Exception {
    if (ftpClient == null) throw new Exception("No client");
    ftpClient.disconnect();
  }


  protected void loginFtpClient() throws Exception {
    if (ftpClient == null) throw new Exception("No client");
    if (!ftpClient.login(username, password)) throw new Exception("Login failed");
  }


  protected TFileFactory getFileFactory() {
    return (TFileFactory) ObjectFactory.getObject(fileFactoryName);
  }


  protected void createRemoteContent() throws Exception {
    TFileFactory fileFactory = getFileFactory();
    TDirectory tdir = fileFactory.dir(remoteUsersPath+"/anonymous");
    tdir.ensureExists();
  }

}
